namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        Firstname = c.String(),
                        Lastname = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Bai viet", "AccountID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Bai viet", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.Bai viet", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Bai viet", new[] { "AccountID" });
            DropForeignKey("dbo.Bai viet", "AccountID", "dbo.Accounts");
            DropColumn("dbo.Bai viet", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
