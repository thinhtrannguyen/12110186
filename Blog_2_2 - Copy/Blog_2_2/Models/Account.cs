﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    
    public class Account
    {
        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Nhập lại địa chỉ Email")]
        public String Email { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự tối đa là 100", MinimumLength = 2)]
        public String Firstname { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự tối đa là 100", MinimumLength = 2)]
        public String Lastname { set; get; }
        public int AccountID { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}