namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bai viet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Bai viet", "Body", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Bai viet", "Body", c => c.String());
            AlterColumn("dbo.Bai viet", "Title", c => c.String());
        }
    }
}
