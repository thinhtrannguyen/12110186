namespace Blog_2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan7 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Bai viet", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Bai viet", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Firstname", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Lastname", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "Lastname", c => c.String());
            AlterColumn("dbo.Accounts", "Firstname", c => c.String());
            AlterColumn("dbo.Accounts", "Email", c => c.String());
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Bai viet", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Bai viet", "Title", c => c.String(nullable: false));
        }
    }
}
