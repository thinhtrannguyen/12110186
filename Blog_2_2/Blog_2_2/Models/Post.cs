﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    [Table("Bai viet")]
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage="Không được bỏ trống trường này")]
        [StringLength(500, ErrorMessage = "Số lượng kí tự trong khoảng 20-500", MinimumLength = 20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        [StringLength(10000, ErrorMessage = "Số lượng kí tự tối thiểu là 50", MinimumLength = 50)]
        public String Body { set; get; }

        [Required(ErrorMessage = "Nhập dd/mm/yyyy")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ")]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Nhập dd/mm/yyyy")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ")]
        public DateTime DateUpdated { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        public int AccountID { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual Account Account { set; get; }
    }
}