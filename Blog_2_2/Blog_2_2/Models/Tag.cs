﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2_2.Models
{
    public class Tag
    {
        [Required]
        public int TagID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống trường này")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự từ 10 đến 100", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}