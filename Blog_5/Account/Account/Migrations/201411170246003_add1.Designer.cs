// <auto-generated />
namespace Account.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class add1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add1));
        
        string IMigrationMetadata.Id
        {
            get { return "201411170246003_add1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
