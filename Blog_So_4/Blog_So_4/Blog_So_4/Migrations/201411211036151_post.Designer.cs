// <auto-generated />
namespace Blog_So_4.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class post : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(post));
        
        string IMigrationMetadata.Id
        {
            get { return "201411211036151_post"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
