﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog2_2.Models;

namespace Blog2_2.Controllers
{
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(Post post, string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                //Tạo list các Tag
                List<Tag> Tags = new List<Tag>();
                //tách các tag theo dấu ,
                string[] TagContent = Content.Split(',');
                //lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content co chua
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có tag rồi thì add thêm post vào 
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới 
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //add vào list các tag
                    Tags.Add(tagExits);
                }
                //gán list tag cho post
                post.Tags = Tags;

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}