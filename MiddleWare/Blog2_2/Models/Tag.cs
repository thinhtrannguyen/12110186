﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Tag
    {
        public int ID { set; get; }
        public String Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}