﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFinancial.Controllers
{
    public class DefaultController : Controller
    {
        //
        // GET: /Default/

        public ActionResult Default()
        {
            //ViewBag.Views = "Đây là trang chủ của MyFinancial";
           
            return View();
        }

        public ActionResult TinKinhTe()
        {
            return View();
        }

        public ActionResult SoThuChi()
        {
            return View();
        }

        public ActionResult TuVanChiTieu()
        {
            return View();
        }

        public ActionResult TinKhuyenMai()
        {
            return View();
        }

        public ActionResult BangXepHang()
        {
            return View();
        }

        public ActionResult TrangChu()
        {
            return View();
        }

        public ActionResult GhiChep()
        {
            return View();
        }

        public ActionResult TinhHinhThuChi()
        {
            return View();
        }

        public ActionResult PhanTichChiTieu()
        {
            return View();
        }

        public ActionResult GioiThieu()
        {
            return View();
        }

        public ActionResult LienHe()
        {
            return View();
        }

        public ActionResult DangKi()
        {
            return View();
        }

        public ActionResult Poster()
        {
            return View();
        }

        
    }
}
