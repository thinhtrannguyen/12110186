﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyFinancial.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Đây là trang chủ của website MyFinancial.com";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Thông tin về MyFinancial.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Thông tin lên hệ.";

            return View();
        }

        public ActionResult GhiChep()
        {
            ViewBag.Message = "Đây là nội dung trang Ghi Chép";
            return View();
        }

        public ActionResult TinhHinhThuChi()
        {
            ViewBag.Message = "Đây là nội dung trang TinhHinhThuChi";
            return View();
        }

        public ActionResult PhanTichChiTieu()
        {
            ViewBag.Message = "Đây là nội dung trang PhanTichChiTieu";
            return View();
        }
    }
}
